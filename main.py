import sys

import tests

from server import settings, app
from blog import urls


def main():
    arg = sys.argv[1]
    if arg == 'run':
        from blog import initial
        try:
            host, port = sys.argv[2], sys.argv[3]
        except Exception as exc:
            host, port = settings.HOST, settings.PORT
        blog_app = app.Application.create_app()
        blog_app.create_server()
        blog_app.set_routes(urls.routes)
        blog_app.run(host, port)
    elif arg == 'test':
        tests.test()


if __name__ == '__main__':
    main()

