import unittest


server_testmodules = ['test_route',
                      'test_http',
                      'test_storage',
                      'test_general',
                      'test_models']


blog_testsmodules = ['test_models',
                     'test_views']


template_testmodules = ['test_inheritance',
                        'test_general',
                        'test_operators']


def test():
    suite = unittest.TestSuite()
    tests = ['server.tests.' + x for x in server_testmodules]
    tests += ['blog.tests.' + x for x in blog_testsmodules]
    tests += ['tengine.tests.' + x for x in template_testmodules]

    for t in tests:
        try:
            mod = __import__(t, globals(), locals(), ['suite', ])
            suitefn = getattr(mod, 'suite')
            suite.addTest(suitefn())
        except (ImportError, AttributeError):
            suite.addTest(unittest.defaultTestLoader.loadTestsFromName(t))

    unittest.TextTestRunner().run(suite)
