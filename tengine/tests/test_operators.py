import unittest

from tengine import operators
from tengine.parser import Parser


class OperatorTestCase(unittest.TestCase):

    def test_correct_for_cycle(self):
        fors = ['{% for x in x %}{% endfor %}',
                '{%  for x in x %} {% endfor %}',
                '{% for x in x   %}{%  endfor  %}',
                '{%   for   x   in xx %}   {%   endfor %}']
        for f in fors:
            self.assertTrue(operators.OperatorType.is_for(operators.OperatorBuilder.build(f)))

    def test_incorrect_for_cycle(self):
        fors = ['{ % for x in x %}{% endfor % }',
                '{%  for x in x % } {% endfor % }',
                '{ %   for   x   in xx % }   { %   endfor % }',
                '{%for x in x %}{% endforr %}',
                '{%  for x in x%} {%endfor%}',
                '{%for x in x%}{%endfor  %}',
                '{%   for   xin xx %}   {%   endfor%}']

        for f in fors:
            self.assertEqual(
                operators.SYNTAX.findall(f),
                []
            )

    def test_correct_if_condition(self):
        fors = ['{% if x %}{% endif %}',
                '{%  if x == 2 %} {% endif %}',
                '{% if a in list   %}{%  endif  %}',
                '{%   if   not a        is        aaaa %}   {%   endif %}']
        for f in fors:
            self.assertTrue(operators.OperatorType.is_if(operators.OperatorBuilder.build(f)))

    def test_incorrect_if_condition(self):
        fors = ['{ % if x!= 100500 %}{% endif % }',
                '{%  if % } {% endif % }',
                '{ %   if rrr % }   { %   endif % }',
                '{%if r is aaa %}{% eendif %}',
                '{%  if x <=tsad%} {%endif%}',
                '{%if x is x%}{%endiff  %}',
                '{%   i fxisis<= as %}   {%   endif%}']

        for f in fors:
            self.assertEqual(
                operators.SYNTAX.findall(f),
                []
            )

    def test_temp(self):
        op = Parser()

        class Last:
            def __init__(self):
                self.variable = '"This is variable of Last class"'

        class Temp:
            def __init__(self):
                self.last = Last()

        args = {'x_list': [1, 2, 3],
                'x': 'This is first x value!',
                'hello': False,
                'some_list': [1, 4, 1, 1],
                'ttt': [],
                'last': Last(),
                'y_list': ['Hello', ',', 'World', 5, '3', '2', '1'],
                'z_list': [Temp(), Temp(), Temp()],
                'itiscorrect': 'good!',
                'four_list': [5, 6, 7],
                'temp': True}

        content = open('tengine/tests/templates/some.html',).read()
        op.parse(content, args)

