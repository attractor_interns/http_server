import unittest

from tengine.engine import TemplateEngineBuilder


class TEngineTestCase(unittest.TestCase):

    def test_inhertitance_and_parsing(self):
        engine = TemplateEngineBuilder.build()

        class Last:
            def __init__(self):
                self.variable = '"This is variable of Last class"'

        class Temp:
            def __init__(self):
                self.last = Last()

        args = {'x_list': [1, 2, 3],
                'x': 'This is first x value!',
                'hello': False,
                'some_list': [1, 4, 1, 1],
                'ttt': [],
                'last': Last(),
                'y_list': ['Hello', ',', 'World', 5, '3', '2', '1'],
                'z_list': [Temp(), Temp(), Temp()],
                'itiscorrect': 'good!',
                'four_list': [5, 6, 7],
                'temp': True}

        filename = 'one/one_operators.html'
        engine.render(filename, args)
