import re
import os
import unittest

from tengine.inheritance import TemplateInheritance, Block


class TestTemplateInheritance(unittest.TestCase):

    def test_path(self):
        path = 'base.html'
        read = TemplateInheritance()._path(path)
        self.assertEqual(
            read,
            os.path.abspath('tengine/tests/templates/base.html')
        )

    def test_content(self):
        content = TemplateInheritance()._get_file_content('base.html')
        self.assertEqual(
            open('tengine/tests/templates/base.html').read(),
            content
        )

    def test_find_blocks(self):
        path = 'base.html'
        read = TemplateInheritance()._path(path)
        starts, ends = TemplateInheritance()._findblocks(open(read).read())

        self.assertEqual(
            len(starts),
            5
        )

    def test_get_extension(self):
        content = open('tengine/tests/templates/index.html').read()
        extension = TemplateInheritance()._get_extension(content)
        self.assertEqual(
            'base.html',
            extension
        )
        content = open('tengine/tests/templates/base.html').read()
        extension = TemplateInheritance()._get_extension(content)
        self.assertEqual(
            None,
            extension
        )

    def test_rx_blocks_start(self):
        path = open('tengine/tests/templates/base.html').read()
        rx_blocks_start = re.findall(r'{% +block +\w+ +%}', path)
        result = ['{% block head %}', '{% block content %}', '{% block attention %}', '{% block index_block %}', '{% block some %}']
        self.assertEqual(
            result,
            rx_blocks_start
        )

    def test_rx_blocks_end(self):
        path = open('tengine/tests/templates/base.html').read()
        rx_blocks_start = re.findall(r'{% +endblock +%}', path)
        result = ['{% endblock %}', '{% endblock %}', '{% endblock %}', '{% endblock %}', '{% endblock %}']
        self.assertEqual(
            result,
            rx_blocks_start
        )

    def test_ascend_to_peak(self):
        filename = os.path.abspath('tengine/tests/templates/one/one.html')
        peak = TemplateInheritance()._ascend_to_peak(filename)
        self.assertEqual(
            peak[-1],
            os.path.abspath('tengine/tests/templates/base.html')
        )

    def test_replace(self):
        replace = TemplateInheritance()._replace('tengine/tests/templates/base.html', '')
        content = open('tengine/tests/templates/base.html').read()
        base_blocks = [Block(content, start, end) for start, end in zip(*TemplateInheritance()._findblocks(content))]
        blocks = 'blocks'
        for base_block in base_blocks:
            try:
                to = next(x for x in blocks if x == base_block)
                content = content.replace(str(base_block), content)
            except StopIteration:
                content = content.replace(str(base_block), '')
        self.assertEqual(
            content,
            replace
        )



