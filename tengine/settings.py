"""
    This file include some template engine settings.
"""


import os


""" BASE_DIR specifies directory with settings.py.
    All modules of the template engine must be in the
    same directory.
"""
BASE_DIR = os.path.dirname(os.path.abspath(__file__))


"""
    TEMPLATE_ROOT_DIR specifies directory with
    all html files.
    You can define your subdirectories in this root directory.
    Ypu can redefine it.
"""
TEMPLATE_ROOT_DIR = os.path.abspath(os.path.join(BASE_DIR, 'tests', 'templates'))

