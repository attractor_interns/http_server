"""This class defines TemplateEngine class.
"""

import os

from tengine import settings

from tengine.parser import Parser
from tengine.inheritance import TemplateInheritance


class TemplateEngine(object):
    """Template engine class that implements inheritance and operator parisng.

       Note:
        if inheritance object has not been defined this class is not
        implementing inheritance. For implementation details see
        following classes and modules.
    """
    def __init__(self, parser, inheritance=None, root_directory=settings.TEMPLATE_ROOT_DIR):
        self.parser = parser
        self.inheritance = inheritance
        self.root_directory = root_directory

    def _path(self, filename):
        """Returns absolute path to the file specified in filename"""
        return os.path.abspath(os.path.join(self.root_directory, filename))

    def render(self, filename, args={}):
        if self.inheritance:
            content = self.inheritance.render(filename)
        else:
            content = open(self._path(filename)).read()
        return self.parser.parse(content, args)


class TemplateEngineBuilder(object):
    """This class defines fabric method that returns TemplateEngine instance
       initialized with parser and inheritance classes.
    """

    @staticmethod
    def build(root_directory=None):
        if root_directory:
            return TemplateEngine(Parser(), TemplateInheritance(root_directory), root_directory)
        else:
            return TemplateEngine(Parser(), TemplateInheritance())




