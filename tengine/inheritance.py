""" This file defines classes for template inheritance implementation.

    Syntax:
        It must be at least one whitespace after {% and
        at least one whitespace before %}. Inside could be any number
        of whitespaces.
        Example is {% extends "path/to/file" %}. For
        more details see at rx_blocks_start and rx_blocks_end regular
        expressions.
"""


import os
import re

from tengine import settings


"""Next lines defines regular expressions for syntatic parsing"""

""" BLOCKS """
rx_blocks_start = re.compile(r'{% +block +\w+ +%}')
rx_blocks_end = re.compile(r'{% +endblock +%}')
rx_blocks_name = re.compile(r'\w+')

""" EXTENSIONS """
rx_extension = re.compile(r'{% +extends +"[\w./]+" +%}')
rx_extension_path = re.compile(r'"[\w./]+"')


class Error(Exception):
    """Base error class for this module"""
    pass


class TemplateInheritanceError(Error):
    """Error raised for incorrect inheritance"""

    def __init__(self, message='Error occurred while trying to inherit'):
        self.message = message

    def __str__(self):
        return '{message}'.format(message=self.message)


class Block(object):
    """ Defines blocks. Stores block content, starting and ending positions.

        Note:
            __init__ gets content that is file content and starting that is
            the name of a starting block in format that rx_blocks_start defines
            i.e. {% block blockname %}.
        Fields:
            name: defines name of the block
            content: defines content that is inside of the block
            start_header: block start header i.e. {% block blah_blah %}
            end_header: block end header i.e. {% endblock %}
    """
    def __init__(self, content, starting, ending):
        self.name = None
        self.content = None
        self.start_header = starting
        self.end_header = ending
        self._process(content, starting, ending)

    def _process(self, content, starting, ending):
        """Retrieves blocks from content using starting and ending of the block in content"""
        try:
            self.name = rx_blocks_name.findall(starting)[-1]
        except:
            raise TemplateInheritanceError('Error occurred while trying to get name of the block')
        start = content.find(starting)
        end = content.find(ending, start) + len(ending)
        self.content = content[start + len(starting):end - len(ending)]

    def __str__(self):
        return '{start}{content}{end}'.format(start=self.start_header,
                                              content=self.content,
                                              end=self.end_header)


class TemplateInheritance(object):
    """This class implements inheritance using recursive function
       _ascend_to_peak.
    """

    def __init__(self, root_directory=settings.TEMPLATE_ROOT_DIR):
        self.root_directory = root_directory

    def render(self, filename):
        """This method implements inheritance

            Args:
                filename (str): name of the html file

            Returns:
                content of the file with resolved extensions of
                parent files (str).
        """
        blocks, peak_filename = self._ascend_to_peak(filename)
        peak_filename = peak_filename if filename != peak_filename else self._path(filename)
        return self._replace(peak_filename, blocks)

    def _replace(self, filename, blocks):
        """Replaces blocks in filename that was derived from children files"""
        content = open(filename).read()
        base_blocks = [Block(content, start, end) for start, end in zip(*self._findblocks(content))]
        for base_block in base_blocks:
            try:
                to = next(x for x in blocks if x.name == base_block.name)
                content = content.replace(str(base_block), to.content)
            except StopIteration:
                content = content.replace(str(base_block), '')
        return content

    def _path(self, filename):
        """Returns absolute path to the file specified in filename"""
        return os.path.abspath(os.path.join(self.root_directory, filename))

    def _get_file_content(self, filename):
        """Returns content of the file"""
        return open(self._path(filename)).read()

    def _get_parent_filepath(self, parent_filename, current_filename):
        """Returns absolute path to parents file that is specified in extends block"""
        parent_filename = self._path(parent_filename)
        return os.path.join(os.path.dirname(parent_filename), current_filename)

    def _get_extension(self, content):
        """Returns name of extending (parent) file otherwise None"""
        extension_block = rx_extension.findall(content)
        if extension_block:
            try:
                extension_path = rx_extension_path.findall(extension_block[0])
                return extension_path[0].replace('"', '')
            except:
                raise TemplateInheritanceError
        return None

    def _findblocks(self, content):
        """Looking for blocks. If blocks were found returns arrays of block starts and ends"""
        starts = rx_blocks_start.findall(content)
        ends = rx_blocks_end.findall(content)
        if len(starts) != len(ends):
            raise TemplateInheritanceError(message='Mismatch of block beginning and ending')
        return starts, ends

    def _ascend_to_peak(self, filename, child_blocks=[]):
        """Implements inheritance with block changings.
           Args:
                child_blocks (list of Block objects): list of blocks that is in child file
                filename (str): name of current file to process
        """
        content = self._get_file_content(filename)
        extension = self._get_extension(content)
        current_blocks = child_blocks
        if extension:
            parent_filename = self._get_parent_filepath(filename, extension)
            starts, ends = self._findblocks(content)
            current_blocks = [Block(content, starting, ending) for starting, ending in zip(starts, ends)]
            for block in current_blocks:
                for child_block in child_blocks:
                    if block.name == child_block.name:
                        block.content = child_block.content
            names = set([x.name for x in child_blocks]) - set([x.name for x in current_blocks])
            for child_block in child_blocks:
                if child_block.name in names:
                    current_blocks.append(child_block)
            current_blocks, filename = self._ascend_to_peak(parent_filename, current_blocks)
        return current_blocks, filename



