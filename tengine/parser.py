"""This module defines algorithm for parsing file.
   This algorithm builds abstract syntax tree for where
   any node that are not leaf is an operator and any
   node that is leaf is an variable or chunk of chars
   from file that is inside | between | outside of
   operators. File content is rendered into the tree
   and than using rendered tree new file is to be
   generated.
   Also this module defined exception class that
   should be used if errors are occurred.
"""


from tengine.operators import OperatorBuilder, OperatorType, Operator, RED_SYM


class Error(Exception):
    """Base error class for this module"""
    pass


class TemplateSyntaxError(Error):
    """Error raised for incorrect syntax"""

    def __init__(self, message='Error occurred while trying to parse content'):
        self.message = message

    def __str__(self):
        return '{message}'.format(message=self.message)


class Node(object):
    """This class defines node of an abstract syntax tree.
       All nodes that are not leaves considered as operators,
       otherwise they are considered data. If node is data
       content stores representation of this data as string.
       If node is operator it points to the object of appropriate
       that could be Var, If or For (see operators module).

       Fields:
            content: delegate object that implements logic of the tree
    """

    def __init__(self, content=None, parent=None):
        self.children = []
        self.parent = parent
        self.content = content

    def process(self, context=None):
        if isinstance(self.content, str):
            return self.content
        return self.content.process(context)

    def add_child(self, content):
        child = Node(content, self)
        self.children.append(child)
        return child

    def is_leaf(self):
        return not self.children

    def is_root(self):
        return not self.parent

    def __str__(self):
        return '{content}'.format(content=self.content)


class Parser(object):
    """This class implements content parsing algorithm"""

    def __str__(self):
        return str(self.node)

    """This method parse content substituting appropriate arguments from args"""
    def parse(self, content, args={}):
        self.stack = []
        self.node = Node()
        self.content = content
        self.conclusion = ''
        self._parse()
        return self._generate(self.node, args)

    """Giving out operators from content and building tree"""
    def _parse(self):
        position = 0
        for exp in Operator.findall(self.content):
            next_position = self.content.find(exp, position)
            if next_position - position:
                self._grow(self.content[position:next_position])
            self._grow(OperatorBuilder.build(exp))
            position += next_position - position + len(exp)
        self.node.add_child(self.content[position:len(self.content)])

    """Checks stack consistency"""
    def syntax_check(self, node):
        if not self.stack:
            raise TemplateSyntaxError('Syntax error occurred. Mismatch of opening and closing blocks.')
        if OperatorType.is_endfor(node):
            if not OperatorType.is_for(self.stack.pop()):
                raise TemplateSyntaxError('Syntax error occurred while trying to parse for loop')
        if OperatorType.is_endif(node):
            operator = self.stack.pop()
            if OperatorType.is_else(operator):
                if not OperatorType.is_if(self.stack.pop()):
                    raise TemplateSyntaxError('Syntax error occurred while trying to parse if condition')
            elif not OperatorType.is_if(operator):
                raise TemplateSyntaxError('Syntax error occurred while trying to parse if condition')

    """Adding new node to tree"""
    def _grow(self, node):
        if isinstance(node, str):
            if not RED_SYM.fullmatch(node):
                self.node.add_child(node)
        elif OperatorType.is_var(node):
            self.node.add_child(node)
        elif OperatorType.is_for(node) or OperatorType.is_if(node):
            self.node = self.node.add_child(node)
            self.stack.append(node)
        elif OperatorType.is_else(node):
            self.node.add_child(node)
            self.stack.append(node)
        elif OperatorType.is_endif(node) or OperatorType.is_endfor(node):
            self.syntax_check(node)
            self.node = self.node.parent

    """Generating output content using tree that was build previously"""
    def _generate(self, node, context):
        if isinstance(node.content, str):
            self.conclusion += node.content
        elif OperatorType.is_for(node.content):
            node.process(context)
            for itr in node.content.iterable:
                local_context = context
                local_context[node.content.variable] = itr
                for child in node.children:
                    self._generate(child, local_context)
        elif OperatorType.is_if(node.content):
            flag = node.process(context)
            if any([OperatorType.is_else(x.content) for x in node.children]):
                for child in node.children:
                    if flag and OperatorType.is_else(child.content):
                        break
                    elif flag:
                        self._generate(child, context)
                if not flag:
                    after = False
                    for child in node.children:
                        if after:
                            self._generate(child, context)
                        else:
                            after = OperatorType.is_else(child.content)
            elif flag:
                for child in node.children:
                    self._generate(child, context)
        elif OperatorType.is_var(node.content):
            self.conclusion += node.process(context)
        if not OperatorType.is_if(node.content) and not OperatorType.is_for(node.content):
            for child in node.children:
                self._generate(child, context)
        return self.conclusion


