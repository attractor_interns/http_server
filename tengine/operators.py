"""This module defines regular expressions, rules
   and classes for implementation conditional and
   cyclic operators.

   Terminology:
    operator: one of following expressions if, for, endif, endfor,
              else and for variables we too use word operator.
    rules:    defined combination of operators, it was defined next
              rules for, if, vars which have next syntax:
              - for rule: {% for x in iterable %}, x - local variable,
                          iterable - any iterable object, for and in are
                          keywords, it can be any number of whitespaces
                          between keywords, local variable and iterable.
              - if rule: {% if condition %}, if is a keyword, condition
                         is a logical condition, as in previous example
                         it can be any number of whitespaces.
              - vars:    {{ var }}, var is a variable that was passed to
                         template engine entry function.
    content:  is a content of file to be rendered.

   Note:
    Regular expressions are used for retrieving operator expressions and
    pyparsing rules are used for retrieving keywords from retrieved
    operator expressions.
"""

import re
import enum

from pyparsing import Word, Suppress, Keyword, Regex, alphas, Or
from pyparsing import ParseException


"""Regular expressions that are used for retrieve operators from content"""
RX_EXPR = r'(not )? *(\w+[\w.]*(?<![.]))( +(==|is|in|<|>|<=|>=|!=) +(\w+[\w.]*(?<![.])))?'
RX_FOR = r'{% +for +\w+ +in +\w+[\w.]*(?<![.]) +%}'
RX_ENDFOR = r'{% +endfor +%}'
RX_IF = r'{% +if +[\w,.!=<> ]+ +%}'
RX_ELSE = r'{% +else +%}'
RX_ENDIF = r'{% +endif +%}'
RX_VARS = r'{{ +\w+[\w.]*(?<![.]) +}}'
SYNTAX = re.compile('|'.join([RX_FOR, RX_ENDFOR, RX_IF, RX_ELSE, RX_ENDIF, RX_VARS]))


"""Rules from pyparsing that are used for retrieving keywords from expressions"""
FOR_RULE = Suppress('{%') + Suppress('for') + Word(alphas) + Suppress('in') + Regex('[a-zA-Z_.]+') + Suppress('%}')
EFOR_RULE = Suppress('{%') + Keyword('endfor') + Suppress('%}')
IF_RULE = Suppress('{%') + Suppress('if') + Regex(RX_EXPR) + Suppress('%}')
ELSE_RULE = Suppress('{%') + Keyword('else') + Suppress('%}')
EIF_RULE = Suppress('{%') + Keyword('endif') + Suppress('%}')
VAR_RULE = Suppress('{{') + Regex('[a-zA-Z_.]+') + Suppress('}}')
RULES = [FOR_RULE, IF_RULE, ELSE_RULE, VAR_RULE, EFOR_RULE, EIF_RULE]
IF_EXP_RULE = Or([Suppress('not') + Regex('[a-zA-Z0-9_.]+') + Suppress(Regex('==|is|in|<|>|<=|>=|!=')) + Regex('[a-zA-Z0-9_.]+'),
                  Regex('[a-zA-Z0-9_.]+') + Suppress(Regex('(==|is|in|<|>|<=|>=|!=)')) + Regex('[a-zA-Z0-9_.]+'),
                  Suppress('not') + Regex('[a-zA-Z0-9_.]+'),
                  Regex('[a-zA-Z0-9_.]+')])

IS_DIGIT = re.compile(r'[+-]?([\d]+[.]?[\d]+|[\d]+)')

RED_SYM = re.compile(r'\n\n *|\n *\n')


class Error(Exception):
    """Base error class for this module"""
    pass


class TemplateOperatorError(Error):
    """Error raised for incorrect syntax"""

    def __init__(self, message='Error occurred while trying to resolve operator'):
        self.message = message

    def __str__(self):
        return '{message}'.format(message=self.message)


class Operator(object):

    @staticmethod
    def findall(content):
        """This method is used for retrieving all operator expressions from file content"""
        return SYNTAX.findall(content)

    def resolve(self, variable, context):
        if '.' in variable:
            names = variable.split('.')[::-1]
            obj = context[names.pop()]
            while len(names):
                name = names.pop()
                obj = getattr(obj, name)
            result = (variable, obj) if not callable(obj) else (variable, obj())
        else:
            if not IS_DIGIT.fullmatch(variable):
                result = variable, context[variable]
            else:
                result = variable, variable
        return result


class For(Operator):
    def __init__(self, variable, iterable):
        self.variable = variable
        self.iterable_key = iterable
        self.iterable = None

    def process(self, context):
        self.iterable_key, self.iterable = self.resolve(self.iterable_key, context)
        return 'for {var} in {itr}'.format(var=self.variable, itr=self.iterable)

    def __str__(self):
        return '{{% for {var} in {itr} %}}'.format(var=self.variable, itr=self.iterable)


class EndFor(Operator):
    def __str__(self):
        return '{% endfor %}'


class If(Operator):
    def __init__(self, condition):
        self.condition = condition
        self.produce = None

    def process(self, context):
        try:
            condition = ' '.join(self.condition.split())
            variables = IF_EXP_RULE.parseString(condition)
            for var in variables:
                key, value = self.resolve(var, context)
                condition = condition.replace(key, str(value))
        except:
            raise TemplateOperatorError('Can not resolve if operator {}'.format(self.__str__()))
        try:
            self.produce = eval(str(condition), {})
        except Exception as exception:
            raise TemplateOperatorError('Error occurred during evaluating if expression ({})'.format(exception))
        return self.produce

    def __str__(self):
        return '{{% if {condition} %}}'.format(condition=self.condition)


class Else(Operator):
    def __str__(self):
        return '{% else %}'


class EndIf(Operator):
    def __str__(self):
        return '{% endif %}'


class Var(Operator):
    def __init__(self, variable):
        self.variable = variable

    def process(self, context):
        try:
            variable, obj = self.resolve(self.variable, context)
        except:
            raise TemplateOperatorError('Can not find variable with name {}'.format(self.variable))
        return str(obj)

    def __str__(self):
        return '{{{{ {var} }}}}'.format(var=self.variable)


class OperatorType(enum.Enum):
    """This class defines constants for operators and methods for operator type check"""
    FOR = For
    ENDFOR = EndFor
    IF = If
    ENDIF = EndIf
    ELSE = Else
    VAR = Var

    @staticmethod
    def is_for(operator):
        return isinstance(operator, For)

    @staticmethod
    def is_endfor(operator):
        return isinstance(operator, EndFor)

    @staticmethod
    def is_if(operator):
        return isinstance(operator, If)

    @staticmethod
    def is_else(operator):
        return isinstance(operator, Else)

    @staticmethod
    def is_endif(operator):
        return isinstance(operator, EndIf)

    @staticmethod
    def is_var(operator):
        return isinstance(operator, Var)


class OperatorBuilder(object):
    """This class defines fabric method that builds for, if or var
       operators depending on expression, if non of these were listed
       it returns None.
    """

    @staticmethod
    def build(expression):
        """This method is used for creating one of defined operator class instance"""
        operator = None
        for rule in RULES:
            try:
                operator = rule.parseString(expression)
                break
            except ParseException:
                pass
        return OperatorBuilder._get_operator(operator, rule)

    @staticmethod
    def _get_operator(content, rule):
        if rule == FOR_RULE:
            return For(*content)
        elif rule == IF_RULE:
            return If(*content)
        elif rule == VAR_RULE:
            return Var(*content)
        elif rule == EFOR_RULE:
            return EndFor()
        elif rule == EIF_RULE:
            return EndIf()
        elif rule == ELSE_RULE:
            return Else()
        return None

