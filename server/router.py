import re

from server.http import HttpResponse, HttpRequest, HttpStatus, HttpRequestError


class Route(object):
    """Connects urls with handler.
    """

    def __init__(self, method, path, handler):
        """Initializes instance

        Args:
            method (str): http method
            path (str): regular expression for mathcing urls
            handler (func): function that is called if url and method was matched
        """
        self.method = method
        self.path = path
        self.handler = handler

    def match(self, method, url):
        """Checks if method and url matches

        Args:
            method (str): http method
            url (str): url from http request

        Returns:
            Route object if success else False
        """
        return self if re.fullmatch(self.path, url) and self.method == method else False

    def handle(self, request):
        """Calls function that is matched to url"""
        return self.handler(request)


class Router(object):
    """Routing class

    Processing of http requests.
    """

    def __init__(self, *args, **kwargs):
        """Initialization method

        Args:
            args (Route):
        """
        self.routes = []
        for arg in args:
            if not isinstance(arg, Route):
                raise TypeError('Incorrect type of arguments')
            self.routes.append(arg)

    def process(self, data):
        """Process http requests"""
        try:
            request = HttpRequest(data.decode('utf-8'))
        except HttpRequestError as error:
            response = HttpResponse(status=HttpStatus.BAD_REQUEST.value,
                                    headers={'Content-Type': 'text/plain'},
                                    body=error.message)
        else:
            response = self._route(request)
        return response.encode()

    def _find(self, request):
        match = None
        for route in self.routes:
            if route.match(request.method, request.url):
                match = route
                break
        return match

    def _route(self, request):
        '''Process http response. Call handler in case of successful url and
        mathod matching. If not url is found in any of pre-defined routes,
        appropriate http response will be returned
        '''

        route = self._find(request)
        try:
            if route:
                response = route.handle(request)
            else:
                response = HttpResponse(status=HttpStatus.NOT_FOUND.value,
                                        headers={'Content-Type': 'text/html'})
        except Exception as exception:
            response = HttpResponse(status=HttpStatus.INTERNAL_SERVER_ERROR.value,
                                    headers={'Content-Type': 'text/html'}, body=exception)
        return response


