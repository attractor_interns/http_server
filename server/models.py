"""This module defines base class for models

    Note:
        All models in applications must be derrived from Model class
        tha defined in this module.
"""

import datetime

from server.storage import Storage


class Model(object):
    """Base class for all models

        Note:
            _instances does not specify number of instances of this class.
            This attribute needs to generate new identifiers. Dont change it!

            _identifier_name defines name of some identifier for instances of
            the classes that are inherit this class.
    """

    _storage = Storage.get_storage()
    _instances = 0
    _identifier_name = 'id'

    @classmethod
    def create(cls, *args, **kwargs):
        """ Creates instance of a class derrived from this class.
            Note:
                only declared attributes will be initialized.
        """
        cls.created_at = None
        instance = cls()
        for kwarg in kwargs:
            setattr(instance, kwarg, kwargs[kwarg])

        instance.__class__._instances += 1
        instance.id = instance.__class__._instances
        instance.created_at = datetime.datetime.now()
        return instance

    def save(self):
        """ Saves instance in storage"""
        key = '{class_name}_{id}'.format(class_name=self.__class__.__name__, id=self.id)
        self.__class__._storage.save(key=key, value=self)
        return self

    def delete(self):
        key = '{class_name}_{id}'.format(class_name=self.__class__.__name__, id=self.id)
        self.__class__._storage.delete(key)

    @classmethod
    def retrieve(cls, id):
        """ Retrieves instance from storage"""
        key = '{class_name}_{id}'.format(class_name=cls.__name__, id=id)
        return cls._storage.retrieve(key)

    @classmethod
    def retrieve_all(cls):
        """Retrieves all instances that was previously saved"""
        array_of_items = []
        for item in cls._storage.storage:
            if cls.__name__ == item.split("_")[0]:
                array_of_items.append(cls._storage.retrieve(item))
        return array_of_items

    def __setattr__(self, name, value):
        """ Checks if attribute was declared in model, if not raises AttributeError"""
        if name != self.__class__._identifier_name and not hasattr(self, name):
            raise AttributeError('{cls} has not attribute {attr}'.format(cls=self.__class__.__name__, attr=name))
        else:
            self.__dict__[name] = value



