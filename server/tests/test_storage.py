import unittest

from server import storage


class StorageTestCase(unittest.TestCase):

    def setUp(self):
        self.db = storage.Storage.get_storage()
        self.key_one = 'this is a test key'
        self.value_one = 'this is the TEST value'
        self.prefix_one = 'somePrefix'

    def test_save_and_retrieve(self):
        self.db.save(key=self.key_one, value=self.value_one, prefix=self.prefix_one)
        self.assertEqual(
            self.db.retrieve(self.prefix_one + self.key_one),
            self.value_one
        )

    def test_key_without_prefix(self):
        value = 'value for key without prefix'
        self.db.save(key=self.key_one, value=value)
        self.db.save(key=self.key_one, value=self.value_one, prefix=self.prefix_one)
        self.assertEqual(
            self.db.retrieve(self.key_one),
            value
        )
        self.assertEqual(
            self.db.retrieve(self.prefix_one + self.key_one),
            self.value_one
        )
        self.assertNotEqual(
            self.db.retrieve(self.prefix_one + self.key_one),
            self.db.retrieve(self.key_one)
        )
        self.assertEqual(
            self.db.retrieve('incorrect key'),
            None
        )

    def test_connect(self):
        self.assertEqual(
            self.db.connect(),
            None
        )

    def test_close(self):
        self.assertEqual(
            self.db.close(),
            None
        )

    def test_not_implemented(self):
        db = storage.AbstractStorage()
        self.assertRaises(
            NotImplementedError,
            db.save,
        )
        self.assertRaises(
            NotImplementedError,
            db.retrieve,
        )


