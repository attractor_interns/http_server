import unittest

from server.models import Model


class ModelTestCase(unittest.TestCase):

    def test_instances_of_classes(self):
        class A(Model):
            pass

        class B(Model):
            pass

        a1 = A.create()
        a2 = A.create()
        a3 = A.create()
        b1 = B.create()
        b2 = B.create()

        self.assertEqual(
            (a1.id, a2.id, a3.id),
            (1, 2, 3)
        )

        self.assertEqual(
            (b1.id, b2.id),
            (1, 2)
        )

        class A(Model):
            pass
        a1 = A.create()

        self.assertEqual(
            a1.id,
            1
        )

        b3 = B.create()
        self.assertEqual(
            b3.id,
            3
        )

    def test_incorrect_attributes_in_create(self):

        class P(Model):
            title = ''
            text = ''

        with self.assertRaises(AttributeError) as context:
            P.create(title='title', text='text', incorrect='error')

        self.assertEqual(
            P.create().id,
            1
        )

    def test_adding_incorrect_attributes(self):

        class P(Model):
            title = ''

        with self.assertRaises(AttributeError) as context:
            p1 = P.create(title='next line raises exception')
            p1.name = 'this attribute has not been defined'

        p2 = P.create()
        p2.title = 'this is correct way to use P class'

        self.assertEqual(
            p2.id,
            2
        )
        self.assertEqual(
            'this is correct way to use P class',
            p2.title
        )

    def test_save_and_retrieve_methods(self):
        class A(Model):
            title = 'hello'
            text = 'World'

        a = A.create()
        a.save()

        a_id = a.id
        del a

        with self.assertRaises(NameError) as context:
            a.id

        self.assertEqual(
            A.retrieve(id=a_id).id,
            a_id
        )

        self.assertEqual(
            A.retrieve(100500),
            None
        )

    def test_retrieve_all(self):
        class R(Model):
            pass

        number = 5
        for i in range(number):
            R.create().save()

        self.assertEqual(
            len(R.retrieve_all()),
            number
        )

        self.assertListEqual(
            sorted([x.id for x in R.retrieve_all()]),
            sorted([1, 2, 3, 4, 5])
        )



















