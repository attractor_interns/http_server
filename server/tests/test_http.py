import unittest

from server import http


class HttpRequestTestCase(unittest.TestCase):

    def test_simple_request(self):
        req = http.HttpRequest('GET /favicon.ico HTTP/1.1\r\nHost: localhost:8888\r\nConnection: keep-alive\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36\r\nAccept: image/webp,image/apng,image/*,*/*;q=0.8\r\nReferer: http://localhost:8888/\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4\r\n\r\n')
        self.assertEqual(
            req.method,
            'GET'
        )
        self.assertEqual(
            req.url,
            '/favicon.ico'
        )

    def test_simple_request_empty_header(self):
        req = http.HttpRequest('GET /empty/header HTTP/1.1\r\nHost: localhost:8888\r\nConnection: \r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36\r\nAccept: image/webp,image/apng,image/*,*/*;q=0.8\r\nReferer: http://localhost:8888/\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4\r\n\r\n')
        self.assertEqual(
            req.method,
            'GET'
        )
        self.assertEqual(
            req.headers.get('Host'),
            'localhost:8888'
        )
        self.assertEqual(
            req.headers.get('Connection', None),
            ''
        )

    def test_request_without_body(self):
        req = http.HttpRequest('GET / HTTP/1.1\r\nHost: alizar.habrahabr.ru\r\n\r\n')
        self.assertEqual(
            req.method,
            'GET'
        )
        self.assertEqual(
            req.url,
            '/'
        )
        self.assertEqual(
            req.headers.get('Host'),
            'alizar.habrahabr.ru'
        )

    def test_request_only_starting(self):
        req = http.HttpRequest('GET / HTTP/1.0\r\n')
        self.assertEqual(
            req.method,
            'GET'
        )
        self.assertEqual(
            req.url,
            '/'
        )
        self.assertEqual(
            req.version,
            'HTTP/1.0'
        )
        self.assertEqual(
            req.body,
            ''
        )
        self.assertEqual(
            req.headers,
            {}
        )

    def test_request_with_whitespaces(self):
        test_body = 'This is a test body that should be tested!'
        req = http.HttpRequest('    GET  /favicon.ico    HTTP/1.1\r\nHost:   localhost:8888\r\nConnection:  keep-alive\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36\r\nAccept: image/webp,image/apng,image/*,*/*;q=0.8\r\nReferer: http://localhost:8888/\r\nAccept-Encoding  :   gzip, deflate, br\r\nAccept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4\r\n\r\n{body}'.format(body=test_body))
        self.assertEqual(
            req.method,
            'GET'
        )
        self.assertEqual(
            req.url,
            '/favicon.ico'
        )
        self.assertEqual(
            req.headers.get('Host'),
            'localhost:8888'
        )
        self.assertEqual(
            req.headers.get('Accept-Encoding'),
            'gzip, deflate, br'
        )
        self.assertEqual(
            req.body,
            test_body
        )

    def test_incorrect_request_control_symbols(self):
        with self.assertRaises(http.HttpRequestError) as context:
            http.HttpRequest('GET /favicon.ico HTTP/1.1\Host: localhost:8888\r\nConnection: keep-alive\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36Accept: image/webp,image/apng,image/*,*/*;q=0.8\r\nReferer: http://localhost:8888/\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4')
        self.assertTrue('Error occurred while trying to parse http request' in str(context.exception))

    def test_incorrect_request(self):
        with self.assertRaises(http.HttpRequestError) as context:
            http.HttpRequest('')
        self.assertTrue('Error occurred while trying to parse http request' in str(context.exception))

        with self.assertRaises(http.HttpRequestError) as context:
            http.HttpRequest('     ')
        self.assertTrue('Error occurred while trying to parse http request' in str(context.exception))

        with self.assertRaises(http.HttpRequestError) as context:
            http.HttpRequest('GET / HTTPaksdjlakd\n\d\s\w\t')
        self.assertTrue('Error occurred while trying to parse http request' in str(context.exception))

        with self.assertRaises(http.HttpRequestError) as context:
            http.HttpRequest(None)
        self.assertTrue('Error occurred while trying to parse http request' in str(context.exception))

        with self.assertRaises(http.HttpRequestError) as context:
            http.HttpRequest('That is incorrect exception')
        self.assertTrue('Error occurred while trying to parse http request' in str(context.exception))

    def test_extract_params_from_GET_request(self):
        request = http.HttpRequest('GET /create_post/?title=hey&content=you HTTP/1.1\r\nHost: localhost:8888\r\nConnection: keep-alive\r\nCache-Control: max-age=0\r\nUpgrade-Insecure-Requests: 1\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4\r\n\r\n')
        self.assertEqual(
            request.params['title'],
            'hey'
        )
        self.assertEqual(
            request.params['content'],
            'you'
        )

    def test_extract_params_with_special_symbols_from_GET_request(self):
        request = http.HttpRequest('GET /create_post/?title=My%20Title&content=My%20Content HTTP/1.1\r\nHost: localhost:8888\r\nConnection: keep-alive\r\nUpgrade-Insecure-Requests: 1\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4\r\n\r\n')
        self.assertEqual(
            request.params['title'],
            'My Title'
        )
        self.assertEqual(
            request.params['content'],
            'My Content'
        )

    def test_extract_params_from_POST_request(self):
        request = http.HttpRequest('POST /create_post HTTP/1.1\r\nHost: localhost:8888\r\nConnection: keep-alive\r\nContent-Length: 42\r\nCache-Control: max-age=0\r\nOrigin: http://localhost:8888\r\nUpgrade-Insecure-Requests: 1\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36\r\nContent-Type: application/x-www-form-urlencoded\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\r\nReferer: http://localhost:8888/new_post\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4\r\n\r\ntitle=hey&content=you&submit=Submit')
        self.assertEqual(
            request.params['title'],
            'hey'
        )
        self.assertEqual(
            request.params['content'],
            'you'
        )

    def test_extract_params_with_special_symbols_from_POST_request(self):
        request = http.HttpRequest('POST /create_post HTTP/1.1\r\nHost: localhost:8888\r\nConnection: keep-alive\r\nContent-Length: 47\r\nCache-Control: max-age=0\r\nOrigin: http://localhost:8888\r\nUpgrade-Insecure-Requests: 1\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36\r\nContent-Type: application/x-www-form-urlencoded\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\r\nReferer: http://localhost:8888/new_post\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4\r\n\r\ntitle=My+Title&content=My-Content&submit=Submit')
        self.assertEqual(
            request.params['title'],
            'My Title'
        )
        self.assertEqual(
            request.params['content'],
            'My-Content'
        )

    def test_do_not_extract_params_from_POST_request_if_inappropriate_content_type(self):
        request = http.HttpRequest('POST /create_post HTTP/1.1\r\nHost: localhost:8888\r\nConnection: keep-alive\r\nContent-Length: 47\r\nCache-Control: max-age=0\r\nOrigin: http://localhost:8888\r\nUpgrade-Insecure-Requests: 1\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36\r\nContent-Type: application\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\r\nReferer: http://localhost:8888/new_post\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4\r\n\r\ntitle=My+Title&content=My-Content&submit=Submit')
        self.assertEqual(
            request.params,
            {}
        )


class HttpResponseTestCase(unittest.TestCase):

    def test_simple_response(self):
        res = http.HttpResponse()
        res.status = 200
        res.headers = {'Host': 'localhost:8888', 'Content-Type': 'application/json'}
        res.body = 'This is the test body. Here could be some information or nothing'

        check1 = ('HTTP/1.1 200 OK\r\n',
                  'Host: localhost:8888\r\n',
                  'Content-Type: application/json\r\n\r\n') + (res.body, )
        check2 = ('HTTP/1.1 200 OK\r\n',
                  'Content-Type: application/json\r\n',
                  'Host: localhost:8888\r\n\r\n') + (res.body, )

        self.assertIn(
            str(res),
            (''.join(check1), ''.join(check2))
        )

    def test_phrase(self):
        res = http.HttpResponse()
        self.assertEqual(res.phrase(), http.HttpStatus.OK.phrase)
        res.status = 400
        self.assertEqual(
            res.phrase(),
            http.HttpStatus.BAD_REQUEST.phrase
        )
        self.assertEqual(
            res.status,
            http.HttpStatus.BAD_REQUEST,
            http.HttpStatus.BAD_REQUEST.value,
        )

    def test_get_headers(self):
        res = http.HttpResponse(headers={'Content-Type': 'text/plain', 'Host': '255.255.255.255', })
        self.assertIn(
            res.get_headers(),
            ('Content-Type: text/plain\r\nHost: 255.255.255.255\r\n',
             'Host: 255.255.255.255\r\nContent-Type: text/plain\r\n')
        )

    def test_get_starting(self):
        res = http.HttpResponse(status=400)
        self.assertEqual(
            res.get_starting(),
            'HTTP/1.1 400 Bad Request'
        )

    def test_get_body(self):
        res = http.HttpResponse()
        self.assertEqual(
            '',
            res.get_body()
        )
