import unittest

from server.router import Route, Router
from server.http import HttpRequestError, HttpResponse, HttpRequest


class RouterTestCase(unittest.TestCase):

    def test_find_pattern(self):
        route = Route('GET', r'^/blog/posts/[0-9]*', None)
        result = route.match('GET', '/blog/posts/1')
        self.assertEqual(
            result,
            route
        )

        self.assertEqual(
            result.method,
            route.method
        )

    def test_init_method(self):
        self.assertRaises(TypeError,
                          Router.__init__,
                          Route(1, 2, 3), Route(1, 2, 3), 1)
        self.assertRaises(TypeError,
                          Router.__init__,
                          Route(1, 2, 3), Route(1, 2, 3), 'string')
        self.assertRaises(TypeError,
                          Router.__init__,
                          Route(1, 2, 3), Route(1, 2, 3), Router())
        self.assertRaises(TypeError,
                          Router.__init__,
                          Route(1, 2, 3), Route(1, 2, 3), dict())

    def test_process_method_on_bad_request(self):
        data = 'GET this is brokenHTTP /brokenpath/ request HTTP/100.500'
        router = Router()
        response = router.process(data.encode())
        expected = HttpResponse(status=400,
                                headers={'Content-Type': 'text/plain'},
                                body=HttpRequestError().message)
        self.assertEqual(
            response,
            expected.encode()
        )

    def test_process_method_on_not_found(self):
        data = 'GET /not/found HTTP/100.500'
        router = Router(Route('GET', r'^/not/find', None),
                        Route('POST', r'^/not/found', None),
                        Route('HEAD', r'^/not/found', None),
                        Route('GET', r'^/found', None),
                        Route('GET', r'^/not', None))
        response = router.process(data.encode())
        expected = HttpResponse(status=404,
                                headers={'Content-Type': 'text/html'})
        self.assertEqual(
            response,
            expected.encode()
        )

    def test_process_method_on_success(self):
        response = HttpResponse(headers={'Cache-Control': 'no-cache', }, body='Flying Spaghetti Monster!')
        router = Router(Route('GET', r'^/', lambda request: response))
        self.assertEqual(
            router.process('GET / HTTP/1.1\r\n'.encode()),
            response.encode()
        )

    def test_process_method_on_internal_error(self):
        router = Router(Route('GET', r'^/', lambda: 'broken handler'))
        expected = HttpResponse(status=500)
        self.assertEqual(
            router._route(HttpRequest('GET / HTTP/1.1\r\n')).status,
            expected.status
        )

    def test_find_method(self):
        route_list = [Route('GET', r'^/not/find', 'handler_0'),
                      Route('POST', r'^/not/found', 'handler_1'),
                      Route('HEAD', r'^/not/found', 'handler_2'),
                      Route('GET', r'^/found', 'handler_3'),
                      Route('GET', r'^/not', 'handler_4')]
        router = Router(*route_list)
        self.assertEqual(
            router._find(HttpRequest('GET /not/find HTTP/1.1')),
            route_list[0]
        )
        self.assertEqual(
            router._find(HttpRequest('POST /not/found HTTP/1.1')),
            route_list[1]
        )
        self.assertEqual(
            router._find(HttpRequest('HEAD /not/found HTTP/1.1')),
            route_list[2]
        )
        self.assertEqual(
            router._find(HttpRequest('GET /found HTTP/1.1')),
            route_list[3]
        )

        self.assertEqual(
            router._find(HttpRequest('GET /not HTTP/1.1')),
            route_list[4]
        )

        self.assertNotEqual(
            router._find(HttpRequest('POST /not/found HTTP/1.1')),
            route_list[2]
        )
        self.assertNotEqual(
            router._find(HttpRequest('GET /foundd HTTP/1.1')),
            route_list[3]
        )
        self.assertNotEqual(
            router._find(HttpRequest('PUT /not HTTP/1.1')),
            route_list[4]
        )
        self.assertEqual(
            router._find(HttpRequest('PUT /no/such/path/in/router HTTP/1.1')),
            None
        )
