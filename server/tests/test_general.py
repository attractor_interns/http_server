import unittest

from server import server, app


class ServerTestCase(unittest.TestCase):
    def setUp(self):
        self.server = server.Server.create_server()


class ApplicationTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app.Application.create_app()

    def test_instanceof_application(self):
        self.assertTrue(isinstance(self.app, app.Application))
