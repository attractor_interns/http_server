from enum import IntEnum
from urllib.parse import unquote_plus


class Error(Exception):
    """Base class for this module"""
    pass


class HttpRequestError(Error):
    """Error raised for incorrect http requests"""

    def __init__(self, message='Error occurred while trying to parse http request'):
        self.message = message

    def __str__(self):
        return '{message}'.format(message=self.message)


class HttpResponse(object):
    """Defines HTTP protocol response object.

    Converts appropriate fields into HTTP response.
    """

    def __init__(self, status=200, headers={}, body=''):
        self.status = status
        self.headers = headers
        self.body = body
        self.version = 'HTTP/1.1'

    def phrase(self):
        """Returns verbal description of HTTP status code"""
        return '' if not self.status else HttpStatus(self.status).phrase

    def get_starting(self):
        """Returns starting line of HTTP response for the object."""
        return '{version} {status} {phrase}'.format(version=self.version, status=self.status, phrase=self.phrase())

    def get_headers(self):
        """Returns headers of HTTP protocol for the object."""
        headers = ''
        for key, value in self.headers.items():
            headers += '{key}: {value}\r\n'.format(key=key, value=value)
        return '{headers}'.format(headers=headers)

    def get_body(self):
        """Returns body of the object."""
        return self.body

    def encode(self):
        """Encodes string to utf-8 representation."""
        return str(self).encode('utf-8')

    def __str__(self):
        return '{starting}\r\n{headers}\r\n{body}'.format(starting=self.get_starting(),
                                                          headers=self.get_headers(),
                                                          body=self.get_body())


class HttpRequest(object):
    """Defines HTTP protocol request object.

    This class accepts raw bytes and transform it
    to the object that represents HTTP request.

    Note:
        Pass raw chunck of bytes to __init__ method to get
        object with next fields: method, headers, body, url,
        version that are corresponding to HTTP request.
    """

    def __init__(self, http_request):
        self.method = None
        self.headers = dict()
        self.body = None
        self.url = None
        self.version = None
        self.params = dict()
        self._parse(http_request)

    def _parse(self, http_request):
        if not http_request:
            raise HttpRequestError()
        try:
            http_request = http_request.strip()
            start_line_and_headers, *self.body = http_request.split('\r\n\r\n')
            self.body = ''.join(self.body)
        except:
            start_line_and_headers, self.body = http_request, ''

        try:
            start_line_and_headers = start_line_and_headers.split('\r\n')
            start_line = start_line_and_headers.pop(0)
            self.method, self.url, self.version = start_line.split()

            for header in start_line_and_headers:
                header = header.split(': ')
                if len(header) < 2:
                    continue
                field = header[0].strip()
                value = header[1].strip()
                self.headers[field] = value
            self._extract_params()

        except Exception as exc:
            raise HttpRequestError

    def _extract_params(self):
        if self.method == "GET" and "?" in self.url:
            unquoted_string = unquote_plus(self.url.split("?")[1])
            for pair in unquoted_string.split("&"):
                self.params[pair.split("=")[0]] = pair.split("=")[1]
        elif self.method == "POST" and "application/x-www-form-urlencoded" in self.headers.get('Content-Type', ''):
            unquoted_string = unquote_plus(self.body)
            for pair in unquoted_string.split("&"):
                split_pair = pair.split('=')
                if len(split_pair) > 1:
                    self.params[split_pair[0]] = split_pair[1]
        elif self.method == "POST" and "multipart/form-data" in self.headers.get('Content-Type', ''):
            unquoted_string = unquote_plus(self.body)
            boundary = self.headers['Content-Type'].split('; ')[1].split('=')[1]
            no_boundary_string = ''.join(unquoted_string.split(boundary)[1: -1]).split('\r\nContent-Disposition: form-data; name="')[1:]
            for pair in no_boundary_string:
                key, value = pair.split('"', 1)
                value = value.replace('\r\n--', '')
                self.params[key] = value


class HttpStatus(IntEnum):
    """Defines HTTP errors

    Note:
        You can use verbal phrases for errors.
        Example: HttpStatus.OK.value instead of 200
    """

    def __new__(cls, value, phrase, description=''):
        obj = int.__new__(cls, value)
        obj._value_ = value

        obj.phrase = phrase
        obj.description = description
        return obj

    # Informational
    CONTINUE = 100, 'Continue', 'Request received, please continue'
    SWITCHING_PROTOCOLS = (101, 'Switching Protocols', 'Switching to new protocol; obey Upgrade header')
    PROCESSING = 102, 'Processing'

    # Success
    OK = 200, 'OK', 'Request fulfilled, document follows'
    CREATED = 201, 'Created', 'Document created, URL follows'
    ACCEPTED = (202, 'Accepted', 'Request accepted, processing continues off-line')
    NON_AUTHORITATIVE_INFORMATION = (203, 'Non-Authoritative Information', 'Request fulfilled from cache')
    NO_CONTENT = 204, 'No Content', 'Request fulfilled, nothing follows'
    RESET_CONTENT = 205, 'Reset Content', 'Clear input form for further input'
    PARTIAL_CONTENT = 206, 'Partial Content', 'Partial content follows'
    MULTI_STATUS = 207, 'Multi-Status'
    ALREADY_REPORTED = 208, 'Already Reported'
    IM_USED = 226, 'IM Used'

    # Redirection
    MULTIPLE_CHOICES = (300, 'Multiple Choices', 'Object has several resources -- see URI list')
    MOVED_PERMANENTLY = (301, 'Moved Permanently', 'Object moved permanently -- see URI list')
    FOUND = 302, 'Found', 'Object moved temporarily -- see URI list'
    SEE_OTHER = 303, 'See Other', 'Object moved -- see Method and URL list'
    NOT_MODIFIED = (304, 'Not Modified', 'Document has not changed since given time')
    USE_PROXY = (305, 'Use Proxy', 'You must use proxy specified in Location to access this resource')
    TEMPORARY_REDIRECT = (307, 'Temporary Redirect', 'Object moved temporarily -- see URI list')
    PERMANENT_REDIRECT = (308, 'Permanent Redirect', 'Object moved temporarily -- see URI list')

    # Client errors
    BAD_REQUEST = (400, 'Bad Request', 'Bad request syntax or unsupported method')
    UNAUTHORIZED = (401, 'Unauthorized', 'No permission -- see authorization schemes')
    PAYMENT_REQUIRED = (402, 'Payment Required', 'No payment -- see charging schemes')
    FORBIDDEN = (403, 'Forbidden', 'Request forbidden -- authorization will not help')
    NOT_FOUND = (404, 'Not Found', 'Nothing matches the given URI')
    METHOD_NOT_ALLOWED = (405, 'Method Not Allowed', 'Specified method is invalid for this resource')
    NOT_ACCEPTABLE = (406, 'Not Acceptable', 'URI not available in preferred format')
    PROXY_AUTHENTICATION_REQUIRED = (407, 'Proxy Authentication Required',
                                     'You must authenticate with this proxy before proceeding')
    REQUEST_TIMEOUT = (408, 'Request Timeout', 'Request timed out; try again later')
    CONFLICT = 409, 'Conflict', 'Request conflict'
    GONE = (410, 'Gone', 'URI no longer exists and has been permanently removed')
    LENGTH_REQUIRED = (411, 'Length Required', 'Client must specify Content-Length')
    PRECONDITION_FAILED = (412, 'Precondition Failed', 'Precondition in headers is false')
    REQUEST_ENTITY_TOO_LARGE = (413, 'Request Entity Too Large', 'Entity is too large')
    REQUEST_URI_TOO_LONG = (414, 'Request-URI Too Long', 'URI is too long')
    UNSUPPORTED_MEDIA_TYPE = (415, 'Unsupported Media Type', 'Entity body in unsupported format')
    REQUESTED_RANGE_NOT_SATISFIABLE = (416, 'Requested Range Not Satisfiable', 'Cannot satisfy request range')
    EXPECTATION_FAILED = (417, 'Expectation Failed', 'Expect condition could not be satisfied')
    UNPROCESSABLE_ENTITY = 422, 'Unprocessable Entity'
    LOCKED = 423, 'Locked'
    FAILED_DEPENDENCY = 424, 'Failed Dependency'
    UPGRADE_REQUIRED = 426, 'Upgrade Required'
    PRECONDITION_REQUIRED = (428, 'Precondition Required', 'The origin server requires the request to be conditional')
    TOO_MANY_REQUESTS = (429, 'Too Many Requests', 'The user has sent too many requests in '
                         'a given amount of time ("rate limiting")')
    REQUEST_HEADER_FIELDS_TOO_LARGE = (431, 'Request Header Fields Too Large',
                                       'The server is unwilling to process the request because its header '
                                       'fields are too large')

    # Server errors
    INTERNAL_SERVER_ERROR = (500, 'Internal Server Error', 'Server got itself in trouble')
    NOT_IMPLEMENTED = (501, 'Not Implemented', 'Server does not support this operation')
    BAD_GATEWAY = (502, 'Bad Gateway', 'Invalid responses from another server/proxy')
    SERVICE_UNAVAILABLE = (503, 'Service Unavailable', 'The server cannot process the request due to a high load')
    GATEWAY_TIMEOUT = (504, 'Gateway Timeout', 'The gateway server did not receive a timely response')
    HTTP_VERSION_NOT_SUPPORTED = (505, 'HTTP Version Not Supported', 'Cannot fulfill request')
    VARIANT_ALSO_NEGOTIATES = 506, 'Variant Also Negotiates'
    INSUFFICIENT_STORAGE = 507, 'Insufficient Storage'
    LOOP_DETECTED = 508, 'Loop Detected'
    NOT_EXTENDED = 510, 'Not Extended'
    NETWORK_AUTHENTICATION_REQUIRED = (511, 'Network Authentication Required',
                                       'The client needs to authenticate to gain network access')
