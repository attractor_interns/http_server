

HOST = 'localhost'
PORT = 8888

MAX_CONNECTIONS = 1

RECV_BUFF_LEN = 8 * 1024

SERVER_TEST_STRING = 'This server is under construction!'
