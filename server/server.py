import socket

from server import settings


class HttpServer(object):
    """This class defines specific algorithm of processing
    TCP packets the to pass all data to router class.

    Note:
        set_router() method should be called before serve_forever method.
    """

    def set_routes(self, router):
        """Setting router object"""
        self.router = router

    def _process(self, request):
        """Calls router process method if router is not defined throws exception"""
        return self.router.process(request)

    def serve_forever(self, host, port):
        """Starts infinite loop

        Args:
            host (str): host ip address
            port (int): port number
        """
        listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        listen_socket.bind((host, port))
        listen_socket.listen(settings.MAX_CONNECTIONS)

        while True:
            client_connection, client_address = listen_socket.accept()
            request = client_connection.recv(settings.RECV_BUFF_LEN)
            client_connection.send(self._process(request))
            client_connection.close()


class Server(object):
    """Generating specific Server object.

    Note:
        Generated object should have serve_forever and set_router methods.
        serve_forever: runs server on defined host and port.
        set_routes: defines what to do with incoming requests.
    """

    @staticmethod
    def create_server():
        return HttpServer()


