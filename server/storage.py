"""This module defines classes for information storing.

    Note:
        AbstractStorage is abstract class that should be subclassed by any
        class that will be used as storage class in application.

        SimpleStorage class defines in-memory storage for
        application. It implements minimal functionality.
        SimpleStorage subclasses Storage class.

        Storage is a class that defines
        fabric method that generates storage object.

        If you want to store some information in storage
        you have to import storage instance from this module.
"""

from server.singleton import SingletonMeta


class AbstractStorage(object):
    """This is an abstract class that should be sublcassed"""

    def connect(self, *args, **kwargs):
        """Should be redefined if necessary

        Returns:
            Must return True if connection was established else False
        """
        pass

    def close(self, *args, **kwargs):
        """Should be redefined if necessary"""
        pass

    def save(self, *args, **kwargs):
        """This method should implement save operation"""
        raise NotImplementedError

    def retrieve(self, *args, **kwargs):
        """This method should redefine retrieve operation"""
        raise NotImplementedError


class SimpleStorage(AbstractStorage):
    """This class implements very simple information storing.

    It redefines save() and retrieve() methods of superclass Storage
    and works just like simple key - value database.
    """

    __metaclass__ = SingletonMeta

    def __init__(self):
        self.storage = dict()

    def save(self, key, value, prefix=''):
        key = ''.join([prefix, str(key)])
        self.storage[key] = value
        return value

    def retrieve(self, key):
        return self.storage.get(key)

    def delete(self, key):
        del self.storage[key]

    def drop(self):
        self.storage = dict()


class Storage(object):

    @staticmethod
    def get_storage():
        return SimpleStorage()

