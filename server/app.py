from server import settings, server


class Application(object):
    """Defining application

    Properties:
        server (Server): implements http protocol
        router (Router): defines routing scheme for application

    Note:
        set_router() method should be called before serve_forever() method.
    """

    @classmethod
    def create_app(cls):
        """Generates instance of Application class"""
        return cls()

    def create_server(self):
        """Generates server object"""
        self.server = server.Server.create_server()

    def set_routes(self, router):
        """Set Router object for application"""
        self.server.set_routes(router)

    def run(self, host=settings.HOST, port=settings.PORT):
        """Starts server"""
        self.server.serve_forever(host, port)


