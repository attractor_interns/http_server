from server.http import HttpResponse, HttpStatus

class Response(HttpResponse):


    def __init__(self):
        # super(Response, self).__init__(status=HttpStatus.OK.value)
        super(Response, self).__init__()
        self.headers={'Content-Type': 'text/html', 'Content-Lang': 'en'}

    def with_status_ok(self):
        self.status = HttpStatus.OK.value
        return self

    def with_status_unauthorized(self):
        self.status = HttpStatus.UNAUTHORIZED.value
        self.body = 'This action requires authorization'
        return self

    def with_status_created(self):
        self.status=HttpStatus.CREATED.value
        return self


    def with_status_bad_request(self):
        self.status=HttpStatus.BAD_REQUEST.value
        self.body = 'You should fill all fields'
        return self

    def with_status_forbidden(self):
        self.status=HttpStatus.FORBIDDEN.value
        self.body = 'You can correct only posts belonged to you'
        return self


class ResponseFactory():

    @staticmethod
    def create_response_with_status(status):
        response = Response()
        if status == 'ok':
            return response.with_status_ok()
        if status == 'unauthorized':
            return response.with_status_unauthorized()
        elif status =='created':
            return response.with_status_created()
        elif status == 'bad request':
            return response.with_status_bad_request()
        elif status == 'forbidden':
            return response.with_status_forbidden()


