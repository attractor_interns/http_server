from blog.models import Post, User


user1 = User.create(login='Vasya', email='pupkin@mail.ru', password='jkljkl', first_name='Vasya', last_name='Pupkin').save()
user2 = User.create(login='Alex', email='gordon@mail.ru', password='jkljkl', first_name='Alex', last_name='Gordon').save()

post1 = Post.create(title="Program analysis", author_id=user1.id, status='published', content="Abstract Syntax Tree").save()
post2 = Post.create(title="Syntax tree",  author_id=user1.id, status='published', content="This distinguishes abstract syntax trees from concrete syntax trees, traditionally designated parse trees, which are typically built by a parser during the source code translation and compiling process. ").save()
post3 = Post.create(title="Motivation",  author_id=user2.id, status='published', content="A tree representation of the abstract syntactic structure of source code written in a programming language.").save()
post4 = Post.create(title="Design",  author_id=user2.id, status='unpublished', content="For instance, grouping parentheses are implicit in the tree structure").save()
post5 = Post.create(title="Usage",  author_id=user1.id, status='unpublished', content="Once built, additional information is added to the AST by means of subsequent processing, e.g., contextual analysis.").save()
