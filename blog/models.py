import re
# import datetime

from server.models import Model


class Post(Model):
    title = None
    content = None
    author_id = None
    status = "unpublished"

    def get_author(self):
        return User.retrieve(id=self.author_id)

    def is_published(self):
        if self.status == 'published':
            return True

    @staticmethod
    def sorted():
        posts = Post.retrieve_all()
        posts = sorted(posts, key=lambda x: x.created_at, reverse=True)
        return posts


class User(Model):
    login = None
    email = None
    password = None
    first_name = None
    last_name = None

    def get_posts(self):
        posts = []
        all_posts = Post.sorted()
        for post in all_posts:
            if post.author_id == self.id:
                posts.append(post)
        return posts

    @staticmethod
    def auth(request):
        login = request.params['login']
        password = request.params['password']
        users = User.retrieve_all()
        for user in users:
            if user.login == login and user.password == password:
                return user

    @staticmethod
    def get_current(request):
        try:
            email = re.search('session_id=\w+@\w+.\w+', request.headers['Cookie']).group().split("=")[1]
            users = User.retrieve_all()
            for user in users:
                if user.email == email:
                    return user
        except:
            return None

