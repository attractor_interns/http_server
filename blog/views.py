from blog.models import Post, User
from blog.response import Response, ResponseFactory
from blog.settings import TEMPLATE_DIR

from tengine.engine import TemplateEngineBuilder

import pdb


def check_fields(request):
    return not '' in request.params.values()


def main_page(request):
    current_user = User.get_current(request)
    posts = Post.sorted()
    response = ResponseFactory.create_response_with_status('ok')
    te = TemplateEngineBuilder.build(TEMPLATE_DIR)
    response.body = te.render('index.html', {'current_user': current_user,
                              'auth': True if current_user else False, 'posts': posts})
    return response


def new_post(request):
    current_user = User.get_current(request)
    if current_user:
        response = ResponseFactory.create_response_with_status('ok')
        te = TemplateEngineBuilder.build(TEMPLATE_DIR)
        response.body = te.render('new_post.html')
        return response
    else:
        response = ResponseFactory.create_response_with_status('unauthorized')
        return response


def create_post(request):
    current_user = User.retrieve('1')
    if check_fields(request):
        post = Post.create(title=request.params['title'],
                           content=request.params['content'],
                           author_id=current_user.id,
                           status=request.params["status"]).save()
        response = ResponseFactory.create_response_with_status('created')
        te = TemplateEngineBuilder.build(TEMPLATE_DIR)
        response.body = te.render('post_created.html', {'post': post})
        return response
    else:
        response = ResponseFactory.create_response_with_status('bad request')
        return response


def login(request):
    user = User.auth(request)
    if user:
        response = ResponseFactory.create_response_with_status('ok')
        response.headers = {'Content-Type': 'text/html',
                            'Content-Lang': 'en',
                            'Set-cookie': 'session_id={email}'.format(email=user.email)}
        te = TemplateEngineBuilder.build(TEMPLATE_DIR)
        response.body = te.render('index.html', {'current_user': user, 'auth': True, 'posts': Post.sorted()})
        return response
    else:
        response = ResponseFactory.create_response_with_status('unauthorized')
        response.body = 'Login or password is incorrect'
        return response


def log_out(request):
    response = ResponseFactory.create_response_with_status('ok')
    response.headers = {'Content-Type': 'text/html', 'Content-Lang': 'en', 'Set-cookie': 'session_id=0'}
    response.body = 'Good Bye'
    return response


def post(request):
    post = Post.retrieve(request.params['id'])
    current_user = User.get_current(request)
    if post and ((current_user and current_user.id == post.author_id) or post.is_published()):
        response = ResponseFactory.create_response_with_status('ok')
        te = TemplateEngineBuilder.build(TEMPLATE_DIR)
        response.body = te.render('post_detail.html', {'post': post})
    else:
        response = ResponseFactory.create_response_with_status('bad request')
        response.body = "You do not have permission to this resource or it does not exist"
    return response


def edit_post(request):
    current_user = User.get_current(request)
    if current_user:
        post = Post.retrieve(request.params['id'])
        is_checked = '"checked"' if post.is_published() else '""'
        if post.author_id == current_user.id:
            response = ResponseFactory.create_response_with_status('ok')
            te = TemplateEngineBuilder.build(TEMPLATE_DIR)
            response.body = te.render('edit_post.html', {'post': post, 'checked': is_checked})
            return response
        else:
            response = ResponseFactory.create_response_with_status('forbidden')
            return response
    else:
        response = ResponseFactory.create_response_with_status('unauthorized')
        return response


def update_post(request):
    id = request.params['id']
    post = Post.retrieve(id)
    post.title, post.content, post.status = request.params['title'], request.params['content'], request.params['status']
    post.save()
    response = ResponseFactory.create_response_with_status('created')
    response.body = '''You have updated post with title:<b>{title}</b>
                       <p> and content:<i>{content}</i>.'''.format(title=post.title, content=post.content)
    return response


def delete_post(request):
    current_user = User.get_current(request)
    post = Post.retrieve(request.params['id'])
    if current_user and post.id in [p.id for p in current_user.get_posts()]:
        post.delete()
        response = ResponseFactory.create_response_with_status('ok')
        response.body = '''You have deleted post with title:<b>{title}</b>
                           and content:<i>{content}</i>.'''.format(title=post.title, content=post.content)
    else:
        response = ResponseFactory.create_response_with_status('forbidden')
        response.body = "You have no appropriate rights to delete this post!"
    return response


def sign_in(request):
    current_user = User.get_current(request)
    if current_user:
        response = ResponseFactory.create_response_with_status('forbidden')
        response.body = 'You are already authorized'
        return response
    else:
        response = ResponseFactory.create_response_with_status('ok')
        te = TemplateEngineBuilder.build(TEMPLATE_DIR)
        response.body = te.render('sign_in.html')
        return response


def sign_up(request):
    current_user = User.get_current(request)
    if current_user:
        response = ResponseFactory.create_response_with_status('forbidden')
        response.body = 'You are already authorized'
        return response
    else:
        response = ResponseFactory.create_response_with_status('ok')
        te = TemplateEngineBuilder.build(TEMPLATE_DIR)
        response.body = te.render('sign_up.html')
        return response


def create_user(request):
    if check_fields(request):
        login = request.params['login']
        email = request.params['email']
        users = User.retrieve_all()
        if login in [u.login for u in users] or email in [u.email for u in users]:
            response = ResponseFactory.create_response_with_status('bad request')
            response.body = "User with such login/email already exists!".format(login, email)
        else:
            user = User.create(login=login,
                               email=email,
                               password=request.params['password'],
                               first_name=request.params['first_name'],
                               last_name=request.params['last_name']).save()
            response = ResponseFactory.create_response_with_status('created')
            response.body = '''You have successfully created account with login:<b>{login}</b>.
                               <p>Please authorize <a href="/sign_in">Log In<a>'''.format(login=user.login)
        return response
    else:
        response = ResponseFactory.create_response_with_status('bad request')
        return response


def users(request):
    response = ResponseFactory.create_response_with_status('ok')
    te = TemplateEngineBuilder.build(TEMPLATE_DIR)
    response.body = te.render('users/users.html', {'users': User.retrieve_all()})
    return response


def user(request):
    current_user = User.get_current(request)
    if current_user:
        user = User.retrieve(request.params['id'])
        posts = user.get_posts()
        response = ResponseFactory.create_response_with_status('ok')
        te = TemplateEngineBuilder.build(TEMPLATE_DIR)
        response.body = te.render('users/user.html', {'user': user, 'posts': posts})
        return response
    else:
        response = ResponseFactory.create_response_with_status('unauthorized')
        return response

def wrong_url(request):
    response = ResponseFactory.create_response_with_status('bad request')
    response.body = "Wrong URL"
    return response

