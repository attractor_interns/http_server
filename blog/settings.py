import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

TEMPLATE_DIR = os.path.abspath(os.path.join(BASE_DIR, 'templates'))
