from blog import views
from server.router import Route, Router


routes = Router(
    Route('GET', r'^/post\?id=\d+', views.post),
    Route('GET', r'^/post/edit\?id=\d+', views.edit_post),
    Route('POST', r'^/post/delete', views.delete_post),
    Route('POST', r'^/post/update', views.update_post),
    Route('GET', r'^/', views.main_page),
    Route('GET', r'^/posts', views.main_page),
    Route('GET', r'^/post/new', views.new_post),
    Route('POST', r'^/post/create', views.create_post),
    Route('GET', r'^/sign_in', views.sign_in),
    Route('GET', r'^/sign_up', views.sign_up),
    Route('GET', r'^/log_out', views.log_out),
    Route('GET', r'^/users', views.users),
    Route('GET', r'^/user\?id=\d+', views.user),
    Route('POST', r'^/login', views.login),
    Route('POST', r'^/user/create', views.create_user),
    Route('GET', r'^/.*', views.wrong_url)
)

