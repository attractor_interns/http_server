import unittest

from server.storage import Storage
from server.http import HttpResponse, HttpRequest, HttpStatus, HttpRequestError

from blog.models import Post, User
from blog import views

import pdb


class ViewsTestCase(unittest.TestCase):


    def setUp(self):
        Storage.get_storage().drop()
        user1 = User.create(login='Vasya', email='pupkin@mail.ru', password='jkljkl', first_name='Vasya', last_name='Pupkin').save()
        user2 = User.create(login='Alex', email='gordon@mail.ru', password='jkljkl', first_name='Alex', last_name='Gordon').save()

        Post.create(title='Program', author_id=user1.id, content='Abstract Syntax Tree').save()
        Post.create(title='Syntax', author_id=user2.id, content='This distinguishes abstract').save()
        Post.create(title='Motivation', author_id=user1.id, content='Abstract Syntax Tree').save()
        Post.create(title='Design', author_id=user1.id, content='This distinguishes abstract').save()


    def test_delete_post_by_another_user(self):
        user1 = User.retrieve_all()[0]
        user2 = User.retrieve_all()[1]

        request = HttpRequest('POST /post/delete HTTP/1.1\r\nHost: localhost:8888\r\nConnection: keep-alive\r\nContent-Length: 18\r\nCache-Control: max-age=0\r\nOrigin: http://localhost:8888\r\nUpgrade-Insecure-Requests: 1\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36\r\nContent-Type: application/x-www-form-urlencoded\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\r\nReferer: http://localhost:8888/\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4\r\nCookie: csrftoken=Ys8gvfYaErxzZvdKfEqpqs3JsQEZ0tdI; session_id={email}\r\n\r\nid={post_id}&Delete=Delete'.format(email=user1.email, post_id=user2.get_posts()[0].id))

        response = views.delete_post(request)

        self.assertTrue(
            "You have no appropriate rights to delete this post!" in response.body
        )

    def test_delete_post_by_current_user(self):
        user1 = User.retrieve_all()[0]

        request = HttpRequest('POST /post/delete HTTP/1.1\r\nHost: localhost:8888\r\nConnection: keep-alive\r\nContent-Length: 18\r\nCache-Control: max-age=0\r\nOrigin: http://localhost:8888\r\nUpgrade-Insecure-Requests: 1\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36\r\nContent-Type: application/x-www-form-urlencoded\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\r\nReferer: http://localhost:8888/\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4\r\nCookie: csrftoken=Ys8gvfYaErxzZvdKfEqpqs3JsQEZ0tdI; session_id={email}\r\n\r\nid={post_id}&Delete=Delete'.format(email=user1.email, post_id=user1.get_posts()[0].id))

        response = views.delete_post(request)

        self.assertTrue(
            "You have deleted post" in response.body
        )
