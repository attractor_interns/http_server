import unittest

from blog.models import Post, User

from server.storage import Storage
from server.models import Model


class ApplicationModelsTestCase(unittest.TestCase):

    def test_retrieve_all_items_of_the_class_from_the_storage(self):
        User._storage.drop()
        user1 = User.create(login='Vasya', email='pupkin@mail.ru', password='jkljkl', first_name='Vasya', last_name='Pupkin').save()
        user2 = User.create(login='Alex', email='gordon@mail.ru', password='jkljkl', first_name='Alex', last_name='Gordon').save()

        Post.create(title='Program', author_id=user1.id, content='Abstract Syntax Tree').save()
        Post.create(title='Syntax', author_id=user2.id, content='This distinguishes abstract').save()

        posts = Post.retrieve_all()
        users = User.retrieve_all()

        self.assertEqual(
            len(posts),
            2
        )
        self.assertEqual(
            len(users),
            2
        )
        self.assertTrue(
            posts[0].title in ['Program', 'Syntax']
        )
        self.assertTrue(
            users[1].login in ['Vasya', 'Alex']

        )


    def test_retrieve_posts_of_selected_user(self):
        user1 = User.create(login='Vasya', email='pupkin@mail.ru', password='jkljkl', first_name='Vasya', last_name='Pupkin').save()
        user2 = User.create(login='Alex', email='gordon@mail.ru', password='jkljkl', first_name='Alex', last_name='Gordon').save()

        post1 = Post.create(title='Program', author_id=user1.id, content='Abstract Syntax Tree').save()
        post2 = Post.create(title='Syntax', author_id=user2.id, content='This distinguishes abstract').save()
        post3 = Post.create(title='Motivation', author_id=user1.id, content='Abstract Syntax Tree').save()
        post4 = Post.create(title='Design', author_id=user1.id, content='This distinguishes abstract').save()

        posts1 = user1.get_posts()
        posts2 = user2.get_posts()

        self.assertEqual(
            len(posts1),
            3
        )

        self.assertTrue(
            posts1[0].title in ['Program', 'Motivation', 'Design']
        )

        self.assertEqual(
            len(posts2),
            1
        )

        self.assertEqual(
            posts2[0].title,
            'Syntax'
        )


    def test_get_author_of_selected_post(self):
        user1 = User.create(login='Vasya', email='pupkin@mail.ru', password='jkljkl', first_name='Vasya', last_name='Pupkin').save()
        user2 = User.create(login='Alex', email='gordon@mail.ru', password='jkljkl', first_name='Alex', last_name='Gordon').save()

        post1 = Post.create(title='Program', author_id=user1.id, content='Abstract Syntax Tree').save()
        post2 = Post.create(title='Syntax', author_id=user2.id, content='This distinguishes abstract').save()
        post3 = Post.create(title='Motivation', author_id=user1.id, content='Abstract Syntax Tree').save()
        post4 = Post.create(title='Design', author_id=user1.id, content='This distinguishes abstract').save()

        self.assertEqual(
            post1.get_author().login,
            user1.login
        )

        self.assertEqual(
            post2.get_author().login,
            user2.login
        )

    def test_singleton_for_models(self):
        class A(Model):
            pass

        class B(Model):
            pass

        class C(Model):
            pass

        self.assertEqual(
            id(A()._storage),
            id(B()._storage),
        )
        self.assertEqual(
            id(B()._storage),
            id(C()._storage),
        )
        self.assertEqual(
            id(C()._storage),
            id(Post.create()._storage)
        )
        self.assertEqual(
            id(User.create()._storage),
            id(Post.create()._storage)
        )






