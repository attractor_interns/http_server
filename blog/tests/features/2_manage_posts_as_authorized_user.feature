Feature: Manage posts as an authorized user

  Scenario: I can create account in blog
    When I fill fields in the registration page
    Then I create account in blog

  Scenario: In order to create account I should fill all fields in the registration page
    When I fill not all fields at the registration page
    Then I do not receive account in blog

  Scenario: In order to create account I should use unique email
    When I fill fields or email that have already been used
    Then I receive message that I can not use this login/email.

  Scenario: In order to create account I should use unique login
    When I fill fields with login that have already been userd
    Then I receive message that I can not use this login/email.

  Scenario: In order to get more access I can be authorized
    When I enter my login and password
    Then I get authorization in the system

  Scenario: As an authorized user I can see full list of posts at home page
    When I visit home page
    Then I see list of published posts

  Scenario: As an authorized user I can read full text of the selected post
    When I click at the title of the selected post
    Then I go to the page where I can read whole text of the post

  Scenario: As an authorized user I can create a new post and mark it as unpublished
    When I create new post and mark it as unpublished
    Then I can find this post in my list of posts
    And I can not find it in the full list of posts

  Scenario: As an authorized user I can create a new post and mark it as published
    When I create new post and mark it as published
    Then I can find this post in my list of posts
    And I can find it in the full list of posts
    And it will appear in the top of the list

  Scenario: As an authorized user I can create new post only if I fill both title and content fields
    When I fill not all fiels at the page of creating post
    Then the post will not be created

  Scenario: As an authorized user I can edit post
    When I have post in a blog and want change its title, content and status. Then I go to the page of editing post
    And change its data

  Scenario: As an authorized user I can delete post
    When I have post in the blog and I want to delete it. Then I go to my profile page
    And delete the post

  Scenario: As a authorized user I can not read selected post if it is not published and it is not mine
      When I go to the page of unpublished post
      Then I see that I can not read this post

  Scenario: As an authorized user I can not read full text of the selected post if it is not published and it is not mine
      When I go to the page belonged to the unpublished post which is not mine
      Then I see that I do not have permission to read this post

  Scenario: As an authorized user I can read full text of the selected post if it is not published but it is mine
      When I go to the page belonged to the unpublished post which is mine
      Then I go to the page where I can read whole text of this unpublished post

  Scenario: As an authorized user I should not input wrong URL
      When I input wrong URL
      Then I see warning message that it is wrong

  Scenario: As an authorized user I can log out from the blog
    When click "Log out" button
    Then I log out from the system


