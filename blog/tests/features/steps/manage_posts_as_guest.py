from nose.tools import assert_equal, assert_true

from server.storage import Storage

import time


@when(u'I visit home page')
def step_impl(context):
    context.driver.get("http://localhost:8888")
    assert_equal(context.driver.title, 'Main Page')


@then(u'I see list of published posts')
def step_impl(context):
    table = context.driver.find_element_by_id('posts')
    list_of_posts = table.find_elements_by_tag_name('tr')
    assert_equal(len(list_of_posts), 3)


@when(u'I click at the title of the selected post')
def step_impl(context):
    post = context.driver.find_element_by_id('post_1')
    post.find_element_by_class_name('title').click()


@then(u'I go to the page where I can read whole text of the post')
def step_impl(context):
    assert_true('Abstract Syntax Tree' in context.driver.page_source)

@when(u'I go to the page of creating post')
def step_impl(context):
    context.driver.get('http://localhost:8888/post/new')

@then(u'I see warning message that I can not do this action')
def step_impl(context):
     assert_true('This action requires authorization' in context.driver.page_source)

@when(u'I go to the page of editing post')
def step_impl(context):
    context.driver.get('http://localhost:8888/post/edit?id=2')

@when(u'I go to the page of unpublished post')
def step_impl(context):
    context.driver.get("http://localhost:8888/post?id=4")

@then(u'I see that I can not read this post')
def step_impl(context):
    assert_true('You do not have permission to this resource' in context.driver.page_source)

@when(u'I input wrong URL')
def step_impl(context):
    context.driver.get("http://localhost:8888/jdlfjd")

@then(u'I see warning message that it is wrong')
def step_impl(context):
    assert_true("Wrong URL" in context.driver.page_source)

@when(u'I input wrong post id')
def step_impl(context):
    context.driver.get('http://localhost:8888/post?id=10')

@then(u'I see warning message that it does not exist or I have no permission to see it')
def step_impl(context):
    assert_true('You do not have permission to this resource or it does not exist' in context.driver.page_source)




if __name__ == "__main__":
    unittest.main()
