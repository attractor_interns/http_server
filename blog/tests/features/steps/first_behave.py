import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

class GetQueries(unittest.TestCase):

    @given(u'I have page with path \'first/test/path/1\'')
    def step_impl(context):
        context.page = "http://localhost:8888/first/test/path/1"

    @when(u'I visit this page')
    def step_impl(context):
        context.driver.get(context.page)

    @then(u'I see this page contains "Hello, this is the first method!!!"')
    def step_impl(context):
        elem = context.driver.find_element_by_tag_name("h1")
        print (elem)
        assert elem.text == "Hello, this is the first method!!!", "No such element!"


if __name__ == "__main__":
    unittest.main()