from nose.tools import assert_equal, assert_true

from blog.models import Post, User

from server.storage import Storage

import time

import pdb



@when(u'I fill not all fields at the registration page')
def step_impl(context):
    context.driver.get("http://localhost:8888/sign_up")
    context.driver.find_element_by_name('login').send_keys('Vano')
    context.driver.find_element_by_name('password').send_keys('jkljkl')
    context.driver.find_element_by_name('email').send_keys('van@mail.ru')
    context.driver.find_element_by_name('first_name').send_keys('Van')
    context.driver.find_element_by_name('submit').click()


@then(u'I do not receive account in blog')
def step_impl(context):
    assert_true('You should fill all fields' in context.driver.page_source)

@when(u'I fill fields in the registration page')
def step_impl(context):
    context.driver.get("http://localhost:8888/sign_up")
    context.driver.find_element_by_name('login').send_keys('Vano')
    context.driver.find_element_by_name('password').send_keys('jkljkl')
    context.driver.find_element_by_name('email').send_keys('van@mail.ru')
    context.driver.find_element_by_name('first_name').send_keys('Van')
    context.driver.find_element_by_name('last_name').send_keys('Vanych')
    context.driver.find_element_by_name('submit').click()


@then(u'I create account in blog')
def step_impl(context):
    assert_true('You have successfully created account' in context.driver.page_source)

@when(u'I enter my login and password')
def step_impl(context):
    context.driver.get("http://localhost:8888/sign_in")
    assert_true('<h1>SIGN IN</h1>' in context.driver.page_source)
    context.driver.find_element_by_name('login').send_keys('Vasya')
    context.driver.find_element_by_name('password').send_keys('jkljkl')
    context.driver.find_element_by_name('submit').click()


@then(u'I get authorization in the system')
def step_impl(context):
    assert_true('Log Out' in context.driver.page_source)

@when(u'I create new post and mark it as unpublished')
def step_impl(context):
    context.driver.get("http://localhost:8888/post/new")
    assert_true('<h1>Create post</h1>' in context.driver.page_source)
    context.driver.find_element_by_name('title').send_keys('Milk Way')
    context.driver.find_element_by_name('content').send_keys('Some cool text')
    context.driver.find_element_by_name('submit').click()
    assert_true('You have already created post' in context.driver.page_source)
    assert_true('Milk Way' in context.driver.page_source)


@then(u'I can find this post in my list of posts')
def step_impl(context):
    context.driver.get('http://localhost:8888/user?id=1')
    assert_true('Milk Way' in context.driver.page_source)
    assert_true('Some cool text' in context.driver.page_source)


@then(u'I can not find it in the full list of posts')
def step_impl(context):
    context.driver.get("http://localhost:8888")
    assert_true('Milk Way' not in context.driver.page_source)
    assert_true('Some cool text' not in context.driver.page_source)


@when(u'I create new post and mark it as published')
def step_impl(context):
    context.driver.get("http://localhost:8888/post/new")
    assert_true('<h1>Create post</h1>' in context.driver.page_source)
    context.driver.find_element_by_name('title').send_keys('Who Knows')
    context.driver.find_element_by_name('content').send_keys('Some nice text')
    context.driver.find_element_by_xpath('/html/body/form/input[4]').click()
    context.driver.find_element_by_name('submit').click()
    assert_true('You have already created post' in context.driver.page_source)
    assert_true('Who Knows' in context.driver.page_source)


@then(u'I can find it in the full list of posts')
def step_impl(context):
    context.driver.get("http://localhost:8888")
    assert_true('Who Knows' in context.driver.page_source)
    assert_true('Some nice text' in context.driver.page_source)

@then(u'it will appear in the top of the list')
def step_impl(context):
    first_post_in_table = context.driver.find_element_by_css_selector('table tr:first-child')
    last_created_post = context.driver.find_element_by_id('post_7')
    assert_equal(first_post_in_table, last_created_post)

@when(u'I fill not all fiels at the page of creating post')
def step_impl(context):
    context.driver.get("http://localhost:8888/post/new")
    assert_true('<h1>Create post</h1>' in context.driver.page_source)
    context.driver.find_element_by_name('title').send_keys('Who Knows')
    context.driver.find_element_by_name('submit').click()


@then(u'the post will not be created')
def step_impl(context):
    assert_true('You should fill all fields' in context.driver.page_source)


@when(u'I have post in a blog and want change its title, content and status. Then I go to the page of editing post')
def step_impl(context):
    context.driver.get('http://localhost:8888/post/edit?id=1')
    assert_true('<h1>Edit post</h1>' in context.driver.page_source)
    context.driver.find_element_by_name('title').clear()
    context.driver.find_element_by_name('title').send_keys('Title is changed')
    context.driver.find_element_by_name('content').clear()
    context.driver.find_element_by_name('content').send_keys('Content is changed')
    context.driver.find_element_by_xpath('/html/body/form/input[5]').click()
    context.driver.find_element_by_name('submit').click()

@when(u'change its data')
def step_impl(context):
    context.driver.get('http://localhost:8888')
    assert_true('Milk Way' not in context.driver.page_source)
    assert_true('Title is changed' not in context.driver.page_source)
    context.driver.get('http://localhost:8888/user?id=1')
    assert_true('Title is changed' in context.driver.page_source)


@when(u'I have post in the blog and I want to delete it. Then I go to my profile page')
def step_impl(context):
    context.driver.get('http://localhost:8888/user?id=1')
    post = context.driver.find_element_by_id("post_2")
    post.find_element_by_name("Delete").click()
    assert_true('You have deleted post with title:<b>Syntax tree</b>' in context.driver.page_source)


@when(u'delete the post')
def step_impl(context):
    context.driver.get('http://localhost:8888/user?id=1')
    assert_true('Syntax tree' not in context.driver.page_source)


@when(u'click "Log out" button')
def step_impl(context):
    context.driver.get('http://localhost:8888')
    context.driver.find_element_by_link_text('Log Out').click()


@then(u'I log out from the system')
def step_impl(context):
    assert_true('Good Bye' in context.driver.page_source)


@when(u'I fill fields or email that have already been used')
def step_impl(context):
    context.driver.get("http://localhost:8888/sign_up")
    context.driver.find_element_by_name('login').send_keys('Vas')
    context.driver.find_element_by_name('password').send_keys('jkljkl')
    context.driver.find_element_by_name('email').send_keys('pupkin@mail.ru')
    context.driver.find_element_by_name('first_name').send_keys('Van')
    context.driver.find_element_by_name('last_name').send_keys('Vano')
    context.driver.find_element_by_name('submit').click()


@when(u'I fill fields with login that have already been userd')
def step_impl(context):
    context.driver.get("http://localhost:8888/sign_up")
    context.driver.find_element_by_name('login').send_keys('Vasya')
    context.driver.find_element_by_name('password').send_keys('jkljkl')
    context.driver.find_element_by_name('email').send_keys('vas@mail.ru')
    context.driver.find_element_by_name('first_name').send_keys('Van')
    context.driver.find_element_by_name('last_name').send_keys('Vano')
    context.driver.find_element_by_name('submit').click()

@then(u'I receive message that I can not use this login/email.')
def step_impl(context):
    assert_true('User with such login/email already exists!' in context.driver.page_source)


@when(u'I go to the page belonged to the unpublished post')
def step_impl(context):
    context.driver.get("http://localhost:8888/post?id=4")


@then(u'I see that I do not have permission to read this post')
def step_impl(context):
    assert_true("You do not have permission to this resource" in context.driver.page_source)


@when(u'I go to the page belonged to the unpublished post which is not mine')
def step_impl(context):
    context.driver.get("http://localhost:8888/post?id=4")

@when(u'I go to the page belonged to the unpublished post which is mine')
def step_impl(context):
    context.driver.get("http://localhost:8888/post?id=5")

@then(u'I go to the page where I can read whole text of this unpublished post')
def step_impl(context):
    assert_true("Usage" in context.driver.page_source)
