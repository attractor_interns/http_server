Feature: Manage posts as a guest

  Scenario: As a guest I want to see full list of posts at home page
      When I visit home page
      Then I see list of published posts

  Scenario: As a guest I want to read full text of the selected post
      When I click at the title of the selected post
      Then I go to the page where I can read whole text of the post

  Scenario: As a guest I can not read full text of the selected post if it is not published
      When I go to the page belonged to the unpublished post
      Then I see that I do not have permission to read this post

  Scenario: As a guest I can not create post
      When I go to the page of creating post
      Then I see warning message that I can not do this action

  Scenario: As a guest I can not edit post
      When I go to the page of editing post
      Then I see warning message that I can not do this action

  Scenario: As a guest I should not input wrong URL
      When I input wrong URL
      Then I see warning message that it is wrong

  Scenario: As a guest I should not input wrong post id
      When I input wrong post id
      Then I see warning message that it does not exist or I have no permission to see it
