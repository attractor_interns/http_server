from selenium import webdriver

# from server import server, app

# import blog

# from imp import reload

# from server.storage import storage

# from server.app import Application

from blog import urls

from subprocess import Popen

process = None

def before_all(context):
    try:
        global process
        process = Popen(['python', 'main.py', 'run'])
    except Exception as inst:
        print(type(inst), inst.args, inst)

    try:
        context.driver = webdriver.Firefox()
        context.driver.implicitly_wait(10)
        context.driver.set_page_load_timeout(10)
        context.driver.maximize_window()
        storage.drop()
    except Exception as inst:
        print(type(inst), inst.args, inst)

def after_all(context):
    context.driver.quit()
    process.kill()