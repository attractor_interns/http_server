## Http Server

### Tools
 - python3.5

#### Project dirs
```

http_server
.
│        behave.ini
│        bitbucket-pipelines.yml
│        geckodriver.log
│        .gitignore
│        requirements.txt
│        README.MD
│        main.py
│        tests.py
│
│
│
└─── server/
│         __init__.py
│         http.py
│         app.py
│         server.py
│         settings.py
│         models.py
│         router.py
│         storage.py
│         │ 
│         └────────────── tests/
│                          │  __init__.py
│                          │   test_http.py
│                          │   test_storage.py
│                          │
│                          └─────────── features/
│                                         │   │
│                                         │   └── pages/
│                                         └── steps/
│ 
└─── blog/
│     │  __init__.py
│     │   initial.py
│     │   response.py
│     │   settings.py
│     │   urls.py
│     │   views.py
│     │   models.py
│     │ 
│     └─────────────  templates/
│     │                     │
│     │                     └─ users/
│     │ 
│     └─────────── tests/
│
│
└─── vhttp
│       bin/
│          activate
│
│
│
└─── tengine/
      │  __init__.py
      │  engine.py
      │  inheritance.py
      │  parser.py
      │  operators.py
      │  settings.py
      │
      └────────────── tests/
                        │
                        │
                        └─── templates/

```


### Installation

> `git clone git clone git@bitbucket.org:attractor_interns/http_server.git`

> `cd http_server`

> `sudo apt-get install python3-pip virtualenvwrapper`

> `mkvirtualenv --python=/usr/bin/python3.5 vhttp`

> `pip install -r requirements.txt`

### How to run/deactivate virtual environment
Running:

> `cd http_server`

> `workon vhhtp`

Deactivating

> `deactivate`

### Hot to install packages
Install only inside virtual environment

> `cd http_server`

> `pip install -r requirements.txt`

If you didn't install pyparsing:

> `sudo apt-get install python3-pyparsing`

### How to run

To run tests

> `python main.py test`

To run feature tests (run server beforehand)

> `behave server/tests/features`

To run server

> `python main.py run [HOST] [PORT]`

HOST and PORT arguments are optional, default values HOST=localhost, PORT=8888

### Using Git

Before every push do

> `flake8 path/to/changed/file.py`

    resolve issues that are listed

Create new branch

> `git checkout -b #issue_number-new-branch-name from-branch-name`

Example:

> `git checkout -b 100500-branch-name develop`

    where 100500 is an issue number

First push after branch was create

> `git push -u origin #issue_number-new-branch-name`

Example:

> `git push -u origin 100500-branch-name`

Then create merge request in bitbucket.com and use `git push` && `git pull`





